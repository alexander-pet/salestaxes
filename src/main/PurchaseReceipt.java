package main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class PurchaseReceipt {

	public static void main(String[] args) {
		String[] orderList = {
                "Input/Input1.txt",
                "Input/Input2.txt",
                "Input/Input3.txt"
        	};
		if (args.length > 0)
			orderList = args;
		
		for (String fileName : orderList) {
			if (new File(fileName).exists()) {
				ShoppingList shoppingList = new ShoppingList();

				try( Stream<String> lines = Files.lines(Paths.get(fileName)) ){ 
					for( String line : (Iterable<String>) lines::iterator ) { 
						shoppingList.addItem(line);
		
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				ReceiptPrinter printer = new ReceiptPrinter();
				SalesTaxCalculator taxCalculator = new SalesTaxCalculator(shoppingList.getItemList());
				for (Item item : shoppingList.getItemList()) {
					printer.printItem(item);
				}
				printer.printTotalTax(taxCalculator.getTotalTax());

				printer.printTotalPrice(taxCalculator.getTaxedPrice());
			}
			else {
				System.out.println("File " + fileName + " does not exist!");
			}
		}

	}
	
	

	
}
