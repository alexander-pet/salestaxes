package main;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Item {

	private boolean imported = false;
	private boolean exemptedTax = false;
	private Double price = 0.0;
	private Double tax = 0.0;
	private Double taxedPrice = 0.0;
	private String order;
	private int quantity = 0;
	private String itemName;
	
	public Item(String order) {
		this.order = order;
		setQuantity();
		setTaxType();
		setPrice();
		setName();
	}
	
	public void setTaxedPrice(Double tPrice) {
		this.taxedPrice = tPrice;
	}

	public Double getTaxes() {
		return tax;
	}
	
	
	public Double getTaxedPrice() {

		return taxedPrice;
	
	}
	
	public String getItemName() {

		return itemName;
	
	}
	
	public boolean imported () {
		return imported;
	}
	
	
	public boolean exempted () {
		return exemptedTax;
	}

	
	public int getQuantity() {
		return quantity;
	}

	
	public Double getPrice() {
		return price;
	}
	
	private void setQuantity() {
		Pattern p = Pattern.compile("(?m)^(\\d+)*");
		Matcher m = p.matcher(order);
		m.find();
		quantity = Integer.parseInt(m.group(0));
		

	}
	
	
	private void setPrice() {
		Pattern p = Pattern.compile("\\d+.\\d+");
		Matcher m = p.matcher(order);
		m.find();
		
		price = Double.parseDouble(m.group(0));
	}

	
	private void setTaxType() {
		Pattern p = Pattern.compile("book|chocolate|pills");
		Matcher m = p.matcher(order);
		boolean exemptItemFound = m.find();
		
		exemptedTax = (exemptItemFound) ? true : false;

		p = Pattern.compile("imported");
		m = p.matcher(order);
		boolean importItemFound = m.find();
		imported = (importItemFound) ? true : false;

	}
	
	private void setName() {
		
		Pattern p = Pattern.compile("([A-Za-z].+(?=\\sat\\s\\d+.\\d+$))");
		Matcher m = p.matcher(order);
		m.find();

		itemName = (!m.group(0).contains("imported")) ? m.group(0) : m.group(0).substring(9);

	}
	
	

	
	
	
	
}
