package main;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class SalesTaxCalculator {

	private Double totalTax = 0.0;
	private Double totalItemPrice = 0.0;
	private Double totalTaxedPrice = 0.0;
	
	public SalesTaxCalculator(ArrayList<Item> items) {
		for (Item item: items) {
			this.totalItemPrice += item.getPrice() * item.getQuantity();
			this.totalTax = totalTax + getTax(item);
			item.setTaxedPrice(round(item.getPrice() * item.getQuantity() + getTax(item)));
		}
		this.totalTaxedPrice = round(totalItemPrice + totalTax);
	}
	
	public Double getTax(Item item) {
		
		Double tax = (!item.exempted()) ? 0.1 : 0.0;
		
		tax += (item.imported()) ? 0.05 : 0.0;
		
		Double roundedTax = (tax.equals(0.0)) ? tax * item.getPrice() : roundTax(new BigDecimal((item.getPrice()*item.getQuantity())*tax), new BigDecimal("0.05"));
		
		return roundedTax;
	}

	private Double round(Double beforeRounded) {
		
		Double afterRounded = Math.round(Math.pow(10.0, 2) * beforeRounded) / Math.pow(10.0, 2);
		
		return afterRounded;
	}

	private Double roundTax(BigDecimal beforeRounded, BigDecimal roundingValue) {
	    if (roundingValue.signum() == 0) {
	        return beforeRounded.doubleValue();
	    }
		
		BigDecimal divided = beforeRounded.divide(roundingValue, 0, RoundingMode.UP);		
		BigDecimal afterRounded = divided.multiply(roundingValue);

		return afterRounded.doubleValue();
		
	}
	
	public Double getTaxedPrice() {
		return round(totalTaxedPrice);
	}
	
	public Double getTotalTax() {
		return round(totalTax);
	}
	
	public Double getTotalItemPrice() {
		return round(totalItemPrice);
	}
	

	
}
