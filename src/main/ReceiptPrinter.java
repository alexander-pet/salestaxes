package main;

public class ReceiptPrinter {

	
	public void printItem (Item item) {
		String outputString = (item.imported()) ? item.getQuantity() + " " + "imported " + item.getItemName() + ": " + item.getTaxedPrice() :
							item.getQuantity() + " " + item.getItemName() + ": " + item.getTaxedPrice();
		System.out.println(outputString);
	}

	public void printTotalTax (Double taxes) {
		System.out.println("SalesTax: " + taxes);

	}

	public void printTotalPrice (Double totalPrice) {
		System.out.println("Total: " + totalPrice + "\n");

	}
	
}
