package main;

import java.util.ArrayList;

public class ShoppingList {
	private ArrayList<Item> itemList = new ArrayList<Item>();
	
	
	public void addItem(String order) {
		Item item = new Item(order);
		itemList.add(item);
	}
	
	public ArrayList<Item> getItemList () {
		return itemList;
	}
	
}
