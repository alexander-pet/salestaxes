package main;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import main.ShoppingList;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)

class ShoppingListTest {

	private ShoppingList shoppingList;
	private String order1;
	private String order2;
	private String order3;
	
	@BeforeAll
	void setUpBeforeClass() throws Exception {
	
		order1 = "1 bottle of perfume at 18.99";
		order2 = "1 imported bottle of perfume at 47.50";
		order3 = "1 book at 12.49";
		
		shoppingList = new ShoppingList();
		
		
	}

	@Test
	void itemListIsEmptyInitialy() {
		assertEquals(true, shoppingList.getItemList().isEmpty());
	}
	
	
	@Test
	void addItemWorks() {
		shoppingList.addItem(order1);
		assertEquals(1, shoppingList.getItemList().size());
		shoppingList.addItem(order2);
		assertEquals(2, shoppingList.getItemList().size());
		shoppingList.addItem(order3);
		assertEquals(3, shoppingList.getItemList().size());
	}

}
