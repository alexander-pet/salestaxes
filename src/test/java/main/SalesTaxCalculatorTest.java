package main;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import main.SalesTaxCalculator;
import main.ShoppingList;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)

class SalesTaxCalculatorTest {

	private String order1;
	private String order2;
	private String order3;
	private ShoppingList shoppingList;
	private SalesTaxCalculator taxCalc;
	
	@BeforeAll
	void setUpBeforeClass() throws Exception {
		
		order1 = "1 bottle of perfume at 18.99";
		order2 = "1 imported bottle of perfume at 47.50";
		order3 = "1 book at 12.49";
		
		shoppingList = new ShoppingList();
		
		shoppingList.addItem(order1);
		shoppingList.addItem(order2);
		shoppingList.addItem(order3);
		
		taxCalc = new SalesTaxCalculator(shoppingList.getItemList());
		
	}

	@Test
	void calculatesTotalPriceCorrect() {
		Double totalPrice = 78.98;
		assertEquals(totalPrice, taxCalc.getTotalItemPrice());
	
	}
	
	
	@Test
	void calculatesTotalTaxCorrect() {
		Double expectedTax = 9.05;
		assertEquals(expectedTax, taxCalc.getTotalTax());
	
	}
	
	
	@Test
	void calculatesTotalTaxedPriceCorrect() {
		Double expectedTaxedPrice = 88.03;
		assertEquals(expectedTaxedPrice, taxCalc.getTaxedPrice());
	
	}

}
