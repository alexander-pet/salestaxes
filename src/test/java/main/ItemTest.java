package main;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import main.Item;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ItemTest {

private Item item;
private int quantity;
private String order;
private Double price;
private boolean imported;
private boolean exempted;

	@BeforeAll
	void setUp() {
		quantity = 12;
		imported = true;
		exempted = true;
		order = "12 imported chocolate box at 12.49";
		price = 12.49;
		item = new Item(order);
	}
	
	@Test
	void testGetQuantity() {
		
		assertEquals(quantity, item.getQuantity());
	}
	
	@Test
	void testGetTaxTypeImported() {

		assertEquals(imported, item.imported());
	}
	
	@Test
	void testGetTaxTypeExempted()  {

		assertEquals(exempted, item.exempted());
	}
	
	@Test
	void testSetPrice()  {

		assertEquals(price, item.getPrice());
	}
	
	@Test
	void testSetItemName()  {
		String expectedName = "chocolate box";
		assertEquals(expectedName, item.getItemName());
	}
	
}
