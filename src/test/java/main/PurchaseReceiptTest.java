package main;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import main.SalesTaxCalculator;
import main.ShoppingList;

class PurchaseReceiptTest {

	private ShoppingList shoppingList;
	private Double expectedTotalTax;
	private Double expectedTotalTaxedPrice;
	
	@BeforeEach
	void setUpBeforeClass() throws Exception {
		shoppingList = new ShoppingList();
	}

	@Test
	void testCorrectOutput1() {
		shoppingList.addItem("1 book at 12.49");
		shoppingList.addItem("1 music CD at 14.99");
		shoppingList.addItem("1 chocolate bar at 0.85");

		SalesTaxCalculator taxCalculator = new SalesTaxCalculator(shoppingList.getItemList());
		
		expectedTotalTax = 1.50;
		expectedTotalTaxedPrice = 29.83;
	
		assertEquals(expectedTotalTax, taxCalculator.getTotalTax());
		assertEquals(expectedTotalTaxedPrice, taxCalculator.getTaxedPrice());
		
	}	
	
	@Test
	void testCorrectOutput2() {
		shoppingList.addItem("1 imported box of chocolates at 10.00");
		shoppingList.addItem("1 imported bottle of perfume at 47.50");

		SalesTaxCalculator taxCalculator = new SalesTaxCalculator(shoppingList.getItemList());
		
		expectedTotalTax = 7.65;
		expectedTotalTaxedPrice = 65.15;
	
		assertEquals(expectedTotalTax, taxCalculator.getTotalTax());
		assertEquals(expectedTotalTaxedPrice, taxCalculator.getTaxedPrice());	}

	@Test
	void testCorrectOutput3() {
		shoppingList.addItem("1 imported bottle of perfume at 27.99");
		shoppingList.addItem("1 bottle of perfume at 18.99");
		shoppingList.addItem("1 packet of headache pills at 9.75");
		shoppingList.addItem("1 box of imported chocolates at 11.25");

		SalesTaxCalculator taxCalculator = new SalesTaxCalculator(shoppingList.getItemList());
		
		expectedTotalTax = 6.70;
		expectedTotalTaxedPrice = 74.68;
	
		assertEquals(expectedTotalTax, taxCalculator.getTotalTax());
		assertEquals(expectedTotalTaxedPrice, taxCalculator.getTaxedPrice());	}
	
	
	
}
